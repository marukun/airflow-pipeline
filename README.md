# Airflow Pipeline for Data Ingestion using Web-Scraping, Hosted in AWS EC2, Store data in MySQL, and Data Visualization using Tableau.

This repository already uses CI/CD to deploy everything. Just make sure all credentials (EC2 credentials, MySQL host, etc.) are correct in the repository variable, and push a commit. If the commits cause no error, merge it into master to deploy to production.

The final dashboard can be accessed via https://ammarchalifah.com/webtoon-insights

The detailed guide on how this Airflow pipeline can be deployed to an EC2 instance are explained in this medium post: https://medium.com/towards-data-science/running-airflow-with-docker-on-ec2-ci-cd-with-gitlab-72326d4baeb4 
